# Passo a passo para este teste

1. Faça o desafio abaixo em seu computador local na linguagem de programação da sua preferência
2. Ao terminar a sua solução faça login no gitlab (ou crie uma conta).
3. Acesse o [projeto através deste link](https://gitlab.com/clubefii-public/awesome-test)
4. Faça um fork do projeto
5. Aplique as suas alterações no novo projeto
6. Faça um commit e dê push para a branch **main**
   *  Você pode fazer isso de duas formas:
      1. Através do [WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/) do projeto no Gitlab (mais simples).
      2. Ou caso você tenha o git instalado e configurado na sua máquina, faça commit e push através do git para o branch
7. Faça um Merge Request a partir do seu projeto para o projeto de origem **clubefii-public/awesome-test**

**Obs:** Caso não consiga terminar o seu algoritmo ou não consiga completar a etapa do Gitlab (passo 2 em diante) me envie o código da sua solução por email.

# O teste

## Somas Possíveis

Faça um algoritmo que dado um valor inteiro (X), imprima todas as somas possíveis para este número utilizando apenas 2 algarismos.
Também deve ser impresso as somas possíveis utilizando 2 algarismos para todos os números inteiros e positivos menores do que X.
A ordem da impressão deve ser decrescente para a ordem dos valores de X conforme o exemplo abaixo.
A impressão das somas possíveis não precisa seguir uma ordem específica (crescente ou decrescente).

### Condições
Onde:
1 < X < 10.000

### Exemplo:

```
Insira o seu número:
7

Número: 7
Somas possíveis: (4, 3),(5, 2),(6, 1)

Número: 6
Somas possíveis: (3, 3),(4, 2),(5, 1)

Número: 5
Somas possíveis: (3, 2),(4, 1)

Número: 4
Somas possíveis: (2, 2),(3, 1)

Número: 3
Somas possíveis: (2, 1)

Número: 2
Somas possíveis: (1, 1)

Número: 1
Somas possíveis:
```
