'''
## Somas Possíveis
Faça um algoritmo que dado um valor inteiro (X), imprima todas as somas possíveis para este número utilizando apenas 2 algarismos.
### Condições
Onde:
1 < X < 10.000

### Exemplo:
```
Insira o seu número:
7

Número: 7
Somas possíveis: (4, 3),(5, 2),(6, 1)
Número: 6
Somas possíveis: (3, 3),(4, 2),(5, 1)
Número: 5
Somas possíveis: (3, 2),(4, 1)
Número: 4
Somas possíveis: (2, 2),(3, 1)
Número: 3
Somas possíveis: (2, 1)
Número: 2
Somas possíveis: (1, 1)
Número: 1
Somas possíveis:
```

'''
import sys
sys.setrecursionlimit(20000)
def somas_possiveis(n):
    '''
    recebe n, calcula as somas possiveis
    '''
    resposta=[]
    if n > 1 and n < 10000:
        for i in range(1, n // 2 + 1):
            resposta.append((n-i, i))
        print ('Número: ', n)
        print('Somas possíveis: ', resposta)
        if (n - 1) == 1:
            print("Fim!")
            return True
        #chamada recursiva
        somas_possiveis(n-1)
    else:
        print("Valor fora do escopo")
        return False

somas_possiveis(int(sys.argv[1]))